FROM registry.gitlab.com/pages/hugo/hugo_extended:0.139.2

RUN apk add --update "nodejs>14" npm yarn

EXPOSE 1313
WORKDIR /src
CMD ["/usr/bin/hugo"]

